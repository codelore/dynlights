package atomicstryker.dynamiclights.common;

import cpw.mods.fml.relauncher.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

import java.util.Iterator;

public class DLTransformer
        implements IClassTransformer {
    private final String classNameWorldObfusc = "aab";

    private final String classNameBlockAccessObfusc = "aak";

    private final String computeBlockLightMethodNameO = "a";

    private final String enumSkyBlockObfusc = "aam";

    private final String classNameWorld = "net.minecraft.world.World";
    private final String blockAccessJava = "net/minecraft/world/IBlockAccess";
    private final String computeBlockLightMethodName = "computeLightValue";

    public byte[] transform(String name, String newName, byte[] bytes) {
        if (name.equals("aab")) {
            return handleWorldTransform(bytes, true);
        }
        if (name.equals("net.minecraft.world.World")) {
            return handleWorldTransform(bytes, false);
        }

        return bytes;
    }

    private byte[] handleWorldTransform(byte[] bytes, boolean obfuscated) {
        System.out.println("**************** Dynamic Lights transform running on World *********************** ");
        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(bytes);
        classReader.accept(classNode, 0);

        Iterator methods = classNode.methods.iterator();
        while (methods.hasNext()) {
            MethodNode m = (MethodNode) methods.next();
            if (m.name.equals(obfuscated ? "a" : "computeLightValue"))
                if (m.desc.equals(obfuscated ? "(IIILaam;)I" : "(IIILnet/minecraft/world/EnumSkyBlock;)I")) {
                    System.out.println("In target method! Patching!");

                    AbstractInsnNode targetNode = null;
                    Iterator iter = m.instructions.iterator();
                    boolean deleting = false;
                    boolean replacing = false;
                    while (iter.hasNext()) {
                        targetNode = (AbstractInsnNode) iter.next();

                        if ((targetNode instanceof VarInsnNode)) {
                            VarInsnNode vNode = (VarInsnNode) targetNode;
                            if (vNode.var == 6) {
                                if (vNode.getOpcode() == 58) {
                                    System.out.println("Bytecode ASTORE 6 case!");
                                    deleting = true;
                                    continue;
                                }
                                if (vNode.getOpcode() == 54) {
                                    System.out.println("Bytecode ISTORE 6 case!");
                                    replacing = true;
                                    targetNode = (AbstractInsnNode) iter.next();
                                    break;
                                }
                            }

                            if ((vNode.var == 7) && (deleting)) {
                                break;
                            }
                        }

                        if (!deleting)
                            continue;
                        System.out.println("Removing " + targetNode);
                        iter.remove();
                    }

                    InsnList toInject = new InsnList();

                    toInject.add(new VarInsnNode(25, 0));
                    toInject.add(new VarInsnNode(21, 5));
                    toInject.add(new VarInsnNode(21, 1));
                    toInject.add(new VarInsnNode(21, 2));
                    toInject.add(new VarInsnNode(21, 3));
                    toInject.add(new MethodInsnNode(184, "atomicstryker/dynamiclights/client/DynamicLights", "getLightValue", "(L" + (obfuscated ? "aak" : "net/minecraft/world/IBlockAccess") + ";IIII)I"));
                    if (replacing) {
                        toInject.add(new VarInsnNode(54, 6));
                    }

                    m.instructions.insertBefore(targetNode, toInject);

                    System.out.println("Patching Complete!");
                    break;
                }
        }

        ClassWriter writer = new ClassWriter(3);
        classNode.accept(writer);
        return writer.toByteArray();
    }
}