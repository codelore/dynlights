package atomicstryker.dynamiclights.client;

import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;

public class DynamicLightSourceContainer {
    private final IDynamicLightSource lightSource;
    private int prevX;
    private int prevY;
    private int prevZ;
    private int x;
    private int y;
    private int z;

    public DynamicLightSourceContainer(IDynamicLightSource light) {
        this.lightSource = light;
        this.x = (this.y = this.z = this.prevX = this.prevY = this.prevZ = 0);
    }

    public boolean onUpdate() {
        Entity ent = this.lightSource.getAttachmentEntity();
        if (!ent.isEntityAlive()) {
            return true;
        }

        if (this.lightSource.getLightLevel() >= 8) {
            if (hasEntityMoved(ent)) {
                ent.worldObj.updateLightByType(EnumSkyBlock.Block, this.x, this.y, this.z);
                ent.worldObj.updateLightByType(EnumSkyBlock.Block, this.prevX, this.prevY, this.prevZ);
            }
        }

        return false;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    public IDynamicLightSource getLightSource() {
        return this.lightSource;
    }

    private boolean hasEntityMoved(Entity ent) {
        int newX = MathHelper.floor_double(ent.posX);
        int newY = MathHelper.floor_double(ent.posY);
        int newZ = MathHelper.floor_double(ent.posZ);

        if ((newX != this.x) || (newY != this.y) || (newZ != this.z)) {
            this.prevX = this.x;
            this.prevY = this.y;
            this.prevZ = this.z;
            this.x = newX;
            this.y = newY;
            this.z = newZ;
            return true;
        }

        return false;
    }

    public boolean equals(Object o) {
        if ((o instanceof DynamicLightSourceContainer)) {
            DynamicLightSourceContainer other = (DynamicLightSourceContainer) o;
            if (other.lightSource == this.lightSource) {
                return true;
            }
        }
        return false;
    }
}