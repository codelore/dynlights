package atomicstryker.dynamiclights.client;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

import java.util.HashMap;
import java.util.Map;


public class ItemConfigHelper {
    private final String SWILDCARD = "*";
    private final int WILDCARD = -1;
    private Map<ItemData, Integer> dataMap;

    public ItemConfigHelper(String configLine, int defaultValue) {
        this.dataMap = new HashMap<ItemData, Integer>();
        for (String s : configLine.split(",")) {
            try {
                String[] duo = s.split("=");
                ItemData item = fromString(duo[0]);
                if (item.startID != 0) {
                    this.dataMap.put(item, duo.length > 1 ? Integer.parseInt(duo[1]) : defaultValue);
                } else {
                    System.out.println("Failed to match String [" + s + "] to a Block or Item, skipping.");
                }
            } catch (Exception e) {
                System.err.println("Error, String [" + s + "] is not a valid Entry, skipping.");
                e.printStackTrace();
            }
        }
    }

    public int retrieveValue(int id, int meta) {
        for (ItemData item : this.dataMap.keySet()) {
            if (item.matches(id, meta)) {
                return this.dataMap.get(item);
            }
        }
        return -1;
    }

    private ItemData fromString(String s) {
        String[] strings = s.split("-");
        int len = strings.length;
        int sid = tryFindingItemID(strings[0]);
        int eid = len > 3 ? tryFindingItemID(strings[1]) : sid;
        int sm = len > 1 ? catchWildcard(strings[1]) : -1;
        int em = len > 2 ? catchWildcard(strings[2]) : sm;
        return new ItemData(sid, eid, sm, em);
    }

    private int tryFindingItemID(String s) {
        try {
            return catchWildcard(s);
        } catch (NumberFormatException e) {
            for (Item item : Item.itemsList) {
                if ((item != null) && (item.getUnlocalizedName().equals(s))) {
                    return item.itemID;
                }
            }
            for (Block block : Block.blocksList) {
                if ((block != null) && (block.getUnlocalizedName().equals(s))) {
                    return block.blockID;
                }
            }
        }
        return 0;
    }

    private int catchWildcard(String s) {
        if (s.equals("*")) {
            return -1;
        }
        return Integer.parseInt(s);
    }

    private class ItemData implements Comparable<ItemData> {
        final int startID;
        final int endID;
        final int startMeta;
        final int endMeta;

        public ItemData(int sid, int eid, int sm, int em) {
            this.startID = sid;
            this.endID = eid;
            this.startMeta = sm;
            this.endMeta = em;
        }

        public String toString() {
            return String.format("%d-%d-%d-%d", this.startID, this.endID, this.startMeta, this.endMeta);
        }

        public boolean matches(int id, int meta) {
            return (isContained(this.startID, this.endID, id)) && (isContained(this.startMeta, this.endMeta, meta));
        }

        private boolean isContained(int s, int e, int i) {
            return ((s == -1) || (i >= s)) && ((e == -1) || (i <= e));
        }

        public int compareTo(ItemData i) {
            return this.startID > i.startID ? 1 : this.startID < i.startID ? -1 : 0;
        }

        public boolean equals(Object o) {
            if ((o instanceof ItemData)) {
                ItemData i = (ItemData) o;
                return (i.startID == this.startID) && (i.endID == this.endID) && (i.startMeta == this.startMeta) && (i.endMeta == this.endMeta);
            }
            return false;
        }

        public int hashCode() {
            return this.startID + this.endID + this.startMeta + this.endMeta;
        }
    }
}