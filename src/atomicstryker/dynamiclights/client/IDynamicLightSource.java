package atomicstryker.dynamiclights.client;

import net.minecraft.entity.Entity;

public abstract interface IDynamicLightSource {
    public abstract Entity getAttachmentEntity();

    public abstract int getLightLevel();
}