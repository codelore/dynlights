package atomicstryker.dynamiclights.client;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;


@Mod(modid = "DynamicLights", name = "Dynamic Lights", version = "1.2.2")
public class DynamicLights {
    private Minecraft mcinstance;
    private static DynamicLights instance;
    private IBlockAccess lastWorld;
    private ConcurrentLinkedQueue<DynamicLightSourceContainer> lastList;
    private ConcurrentHashMap worldLightsMap;
    private boolean globalLightsOff;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        instance = this;
        this.globalLightsOff = false;
        this.mcinstance = FMLClientHandler.instance().getClient();
        this.worldLightsMap = new ConcurrentHashMap();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);

        KeyBinding[] key = {new KeyBinding("Dynamic Lights toggle", 38)};
        boolean[] repeat = {false};
        KeyBindingRegistry.registerKeyBinding(new LightsOnOffKey(key, repeat));
    }

    public static boolean globalLightsOff() {
        return instance.globalLightsOff;
    }

    public static int getLightValue(IBlockAccess world, int blockID, int x, int y, int z) {
        int vanillaValue = Block.blocksList[blockID] != null ? Block.blocksList[blockID].getLightValue(world, x, y, z) : 0;

        if ((instance == null) || (instance.globalLightsOff) || ((world instanceof WorldServer))) {
            return vanillaValue;
        }

        if ((!world.equals(instance.lastWorld)) || (instance.lastList == null)) {
            instance.lastWorld = world;
            instance.lastList = ((ConcurrentLinkedQueue) instance.worldLightsMap.get(world));
        }

        if ((instance.lastList != null) && (!instance.lastList.isEmpty())) {
            for (DynamicLightSourceContainer light : instance.lastList) {
                if (light.getX() == x) {
                    if (light.getY() == y) {
                        if (light.getZ() == z) {
                            int dynamicValue = light.getLightSource().getLightLevel();
                            if (dynamicValue > vanillaValue) {
                                return dynamicValue;
                            }
                        }
                    }
                }
            }
        }

        return vanillaValue;
    }

    public static void addLightSource(IDynamicLightSource lightToAdd) {
        if (lightToAdd.getAttachmentEntity() != null) {
            if (lightToAdd.getAttachmentEntity().isEntityAlive()) {
                DynamicLightSourceContainer newLightContainer = new DynamicLightSourceContainer(lightToAdd);
                ConcurrentLinkedQueue lightList = (ConcurrentLinkedQueue) instance.worldLightsMap.get(lightToAdd.getAttachmentEntity().worldObj);
                if (lightList != null) {
                    if (!lightList.contains(newLightContainer)) {
                        lightList.add(newLightContainer);
                    } else {
                        System.out.println("Cannot add Dynamic Light: Attachment Entity is already registered!");
                    }
                } else {
                    lightList = new ConcurrentLinkedQueue();
                    lightList.add(newLightContainer);
                    instance.worldLightsMap.put(lightToAdd.getAttachmentEntity().worldObj, lightList);
                }
            } else {
                System.err.println("Cannot add Dynamic Light: Attachment Entity is dead!");
            }
        } else {
            System.err.println("Cannot add Dynamic Light: Attachment Entity is null!");
        }
    }

    public static void removeLightSource(IDynamicLightSource lightToRemove) {
        if ((lightToRemove != null) && (lightToRemove.getAttachmentEntity() != null)) {
            World world = lightToRemove.getAttachmentEntity().worldObj;
            if (world != null) {
                DynamicLightSourceContainer iterContainer = null;
                ConcurrentLinkedQueue lightList = (ConcurrentLinkedQueue) instance.worldLightsMap.get(world);
                if (lightList != null) {
                    Iterator iter = lightList.iterator();
                    while (iter.hasNext()) {
                        iterContainer = (DynamicLightSourceContainer) iter.next();
                        if (!iterContainer.getLightSource().equals(lightToRemove))
                            continue;
                        iter.remove();
                    }

                    if (iterContainer != null) {
                        world.updateLightByType(EnumSkyBlock.Block, iterContainer.getX(), iterContainer.getY(), iterContainer.getZ());
                    }
                }
            }
        }
    }

    private class LightsOnOffKey extends KeyBindingRegistry.KeyHandler {
        private EnumSet tickTypes = EnumSet.of(TickType.CLIENT);

        public LightsOnOffKey(KeyBinding[] keyBindings, boolean[] repeatings) {
            super(keyBindings, repeatings);
        }

        public String getLabel() {
            return "DynamicLightsKey";
        }

        public void keyDown(EnumSet types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {
        }

        public void keyUp(EnumSet types, KeyBinding kb, boolean tickEnd) {
            if ((tickEnd) && (DynamicLights.this.mcinstance.currentScreen == null)) {
                DynamicLights.this.globalLightsOff = !DynamicLights.this.globalLightsOff;
                DynamicLights.this.mcinstance.ingameGUI.getChatGUI().printChatMessage("Dynamic Lights globally " + (DynamicLights.this.globalLightsOff ? "off" : "on"));

                World world = DynamicLights.this.mcinstance.theWorld;
                if (world != null) {
                    ConcurrentLinkedQueue worldLights = (ConcurrentLinkedQueue) DynamicLights.this.worldLightsMap.get(world);
                    if (worldLights != null) {
                        Iterator iter = worldLights.iterator();
                        while (iter.hasNext()) {
                            DynamicLightSourceContainer c = (DynamicLightSourceContainer) iter.next();
                            world.updateLightByType(EnumSkyBlock.Block, c.getX(), c.getY(), c.getZ());
                        }
                    }
                }
            }
        }

        public EnumSet ticks() {
            return this.tickTypes;
        }
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if (DynamicLights.this.mcinstance.theWorld != null) {
                ConcurrentLinkedQueue worldLights = (ConcurrentLinkedQueue) DynamicLights.this.worldLightsMap.get(DynamicLights.this.mcinstance.theWorld);

                if (worldLights != null) {
                    Iterator iter = worldLights.iterator();
                    while (iter.hasNext()) {
                        DynamicLightSourceContainer tickedLightContainer = (DynamicLightSourceContainer) iter.next();
                        if ((tickedLightContainer != null) && (tickedLightContainer.onUpdate())) {
                            iter.remove();
                            DynamicLights.this.mcinstance.theWorld.updateLightByType(EnumSkyBlock.Block, tickedLightContainer.getX(), tickedLightContainer.getY(), tickedLightContainer.getZ());
                        }
                    }
                }
            }
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights";
        }
    }
}