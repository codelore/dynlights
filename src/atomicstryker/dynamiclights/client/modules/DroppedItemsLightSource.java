package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import atomicstryker.dynamiclights.client.ItemConfigHelper;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;


@Mod(modid = "DynamicLights_dropItems", name = "Dynamic Lights on ItemEntities", version = "1.0.4", dependencies = "required-after:DynamicLights")
public class DroppedItemsLightSource {
    private Minecraft mcinstance;
    private long nextUpdate;
    private long updateInterval;
    private ArrayList trackedItems;
    private Thread thread;
    private boolean threadRunning;
    private ItemConfigHelper itemsMap;
    private ItemConfigHelper notWaterProofItems;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        config.load();

        Property itemsList = config.get("general", "LightItems", "50,89=12,348=10,91,327,76=10,331=10,314=14");
        itemsList.comment = "Item IDs that shine light when dropped in the World.";
        this.itemsMap = new ItemConfigHelper(itemsList.getString(), 15);

        Property updateI = config.get("general", "update Interval", 1000);
        updateI.comment = "Update Interval time for all Item entities in milliseconds. The lower the better and costlier.";
        this.updateInterval = updateI.getInt();

        Property notWaterProofList = config.get("general", "TurnedOffByWaterItems", "50,327");
        notWaterProofList.comment = "Item IDs that do not shine light when dropped and in water, have to be present in LightItems.";
        this.notWaterProofItems = new ItemConfigHelper(notWaterProofList.getString(), 1);

        config.save();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        this.mcinstance = FMLClientHandler.instance().getClient();
        this.nextUpdate = System.currentTimeMillis();
        this.trackedItems = new ArrayList();
        this.threadRunning = false;
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
    }

    private int getLightFromItemStack(ItemStack stack) {
        if (stack != null) {
            int r = this.itemsMap.retrieveValue(stack.itemID, stack.getItemDamage());
            return r < 0 ? 0 : r;
        }
        return 0;
    }

    private class EntityItemAdapter
            implements IDynamicLightSource {
        private EntityItem entity;
        private int lightLevel;
        private boolean enabled;
        private boolean notWaterProof;

        public EntityItemAdapter(EntityItem eI) {
            this.lightLevel = 0;
            this.enabled = false;
            this.entity = eI;
            this.notWaterProof = (DroppedItemsLightSource.this.notWaterProofItems.retrieveValue(eI.getEntityItem().itemID, eI.getEntityItem().getItemDamage()) == 1);
        }

        public void onTick() {
            if (this.entity.isBurning()) {
                this.lightLevel = 15;
            } else {
                this.lightLevel = DroppedItemsLightSource.this.getLightFromItemStack(this.entity.getEntityItem());

                if ((this.notWaterProof) && (this.entity.worldObj.getBlockMaterial(MathHelper.floor_double(this.entity.posX), MathHelper.floor_double(this.entity.posY), MathHelper.floor_double(this.entity.posZ)) == Material.water)) {
                    this.lightLevel = 0;
                }
            }

            if ((!this.enabled) && (this.lightLevel > 8)) {
                enableLight();
            } else if ((this.enabled) && (this.lightLevel < 9)) {
                disableLight();
            }
        }

        private void enableLight() {
            DynamicLights.addLightSource(this);
            this.enabled = true;
        }

        private void disableLight() {
            DynamicLights.removeLightSource(this);
            this.enabled = false;
        }

        public Entity getAttachmentEntity() {
            return this.entity;
        }

        public int getLightLevel() {
            return (this.notWaterProof) && (this.entity.isInWater()) ? 0 : this.lightLevel;
        }
    }

    private class EntityListChecker extends Thread {
        private final Object[] list;

        public EntityListChecker(List input) {
            this.list = input.toArray();
        }

        public void run() {
            ArrayList newList = new ArrayList();

            for (Object o : this.list) {
                Entity ent = (Entity) o;

                if ((!(ent instanceof EntityItem)) || (!ent.isEntityAlive())) {
                    continue;
                }
                boolean found = false;
                Iterator iter = DroppedItemsLightSource.this.trackedItems.iterator();
                DroppedItemsLightSource.EntityItemAdapter adapter = null;
                while (iter.hasNext()) {
                    adapter = (DroppedItemsLightSource.EntityItemAdapter) iter.next();
                    if (!adapter.getAttachmentEntity().equals(ent))
                        continue;
                    adapter.onTick();
                    newList.add(adapter);
                    found = true;
                    iter.remove();
                }

                if (found) {
                    continue;
                }
                adapter = new DroppedItemsLightSource.EntityItemAdapter((EntityItem) ent);
                adapter.onTick();
                newList.add(adapter);
            }

            trackedItems = newList;
            threadRunning = false;
        }
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if ((DroppedItemsLightSource.this.mcinstance.theWorld != null) && (System.currentTimeMillis() > DroppedItemsLightSource.this.nextUpdate) && (!DynamicLights.globalLightsOff())) {
                nextUpdate = System.currentTimeMillis() + DroppedItemsLightSource.this.updateInterval;

                if (!DroppedItemsLightSource.this.threadRunning) {
                    thread = new DroppedItemsLightSource.EntityListChecker(DroppedItemsLightSource.this.mcinstance.theWorld.loadedEntityList);
                    DroppedItemsLightSource.this.thread.setPriority(1);
                    DroppedItemsLightSource.this.thread.start();
                    threadRunning = true;
                }
            }
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights_dropItems";
        }
    }
}
