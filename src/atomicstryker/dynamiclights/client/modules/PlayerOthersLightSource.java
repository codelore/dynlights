package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import atomicstryker.dynamiclights.client.ItemConfigHelper;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;


@Mod(modid = "DynamicLights_otherPlayers", name = "Dynamic Lights Other Player Light", version = "1.0.4", dependencies = "required-after:DynamicLights")
public class PlayerOthersLightSource {
    private Minecraft mcinstance;
    private long nextUpdate;
    private long updateInterval;
    private ArrayList trackedPlayers;
    private Thread thread;
    private boolean threadRunning;
    private ItemConfigHelper itemsMap;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        config.load();

        Property itemsList = config.get("general", "LightItems", "50,89=12,348=10,91,327,76=10,331=10,314=14");
        itemsList.comment = "Item IDs that shine light while held. Armor Items also work when worn. [ONLY ON OTHERS] Syntax: ItemID[-MetaValue]:LightValue, seperated by commas";
        this.itemsMap = new ItemConfigHelper(itemsList.getString(), 15);

        Property updateI = config.get("general", "update Interval", 1000);
        updateI.comment = "Update Interval time for all other player entities in milliseconds. The lower the better and costlier.";
        this.updateInterval = updateI.getInt();

        config.save();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        this.mcinstance = FMLClientHandler.instance().getClient();
        this.nextUpdate = System.currentTimeMillis();
        this.trackedPlayers = new ArrayList();
        this.threadRunning = false;
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
    }

    private int getLightFromItemStack(ItemStack stack) {
        int xLight = getXLightFromItemStack(stack);
        if (xLight > 0)
            return xLight;

        if (stack != null) {
            int r = this.itemsMap.retrieveValue(stack.itemID, stack.getItemDamage());
            return r < 0 ? 0 : r;
        }
        return 0;
    }

    private int getXLightFromItemStack(ItemStack stack) {
        if (stack == null)
            return 0;

        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null && compound.hasKey("xlight")) {
            NBTTagCompound xlight = compound.getCompoundTag("xlight");
            return xlight.getInteger("level");
        }
        return 0;
    }

    private class OtherPlayerAdapter
            implements IDynamicLightSource {
        private EntityPlayer player;
        private int lightLevel;
        private boolean enabled;

        public OtherPlayerAdapter(EntityPlayer p) {
            this.lightLevel = 0;
            this.enabled = false;
            this.player = p;
        }

        public void onTick() {
            int prevLight = this.lightLevel;

            this.lightLevel = PlayerOthersLightSource.this.getLightFromItemStack(this.player.getCurrentEquippedItem());
            for (ItemStack armor : this.player.inventory.armorInventory) {
                this.lightLevel = Math.max(this.lightLevel, PlayerOthersLightSource.this.getLightFromItemStack(armor));
            }

            if ((prevLight != 0) && (this.lightLevel != prevLight)) {
                this.lightLevel = 0;
            } else if (this.player.isBurning()) {
                this.lightLevel = 15;
            }

            if ((!this.enabled) && (this.lightLevel > 8)) {
                enableLight();
            } else if ((this.enabled) && (this.lightLevel < 9)) {
                disableLight();
            }
        }

        private void enableLight() {
            DynamicLights.addLightSource(this);
            this.enabled = true;
        }

        private void disableLight() {
            DynamicLights.removeLightSource(this);
            this.enabled = false;
        }

        public Entity getAttachmentEntity() {
            return this.player;
        }

        public int getLightLevel() {
            return this.lightLevel;
        }
    }

    private class OtherPlayerChecker extends Thread {
        private final Object[] list;

        public OtherPlayerChecker(List input) {
            this.list = input.toArray();
        }

        public void run() {
            ArrayList newList = new ArrayList();

            for (Object o : this.list) {
                Entity ent = (Entity) o;

                if ((!(ent instanceof EntityOtherPlayerMP)) || (!ent.isEntityAlive())) {
                    continue;
                }
                boolean found = false;
                Iterator iter = PlayerOthersLightSource.this.trackedPlayers.iterator();
                PlayerOthersLightSource.OtherPlayerAdapter adapter = null;
                while (iter.hasNext()) {
                    adapter = (PlayerOthersLightSource.OtherPlayerAdapter) iter.next();
                    if (!adapter.getAttachmentEntity().equals(ent))
                        continue;
                    adapter.onTick();
                    newList.add(adapter);
                    found = true;
                    iter.remove();
                }

                if (found) {
                    continue;
                }
                adapter = new PlayerOthersLightSource.OtherPlayerAdapter((EntityPlayer) ent);
                adapter.onTick();
                newList.add(adapter);
            }

            trackedPlayers = newList;
            threadRunning = false;
        }
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if ((PlayerOthersLightSource.this.mcinstance.theWorld != null) && (System.currentTimeMillis() > PlayerOthersLightSource.this.nextUpdate) && (!DynamicLights.globalLightsOff())) {
                nextUpdate = System.currentTimeMillis() + PlayerOthersLightSource.this.updateInterval;

                if (!PlayerOthersLightSource.this.threadRunning) {
                    thread = new PlayerOthersLightSource.OtherPlayerChecker(PlayerOthersLightSource.this.mcinstance.theWorld.loadedEntityList);
                    PlayerOthersLightSource.this.thread.setPriority(1);
                    PlayerOthersLightSource.this.thread.start();
                    threadRunning = true;
                }
            }
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights_otherPlayers";
        }
    }
}