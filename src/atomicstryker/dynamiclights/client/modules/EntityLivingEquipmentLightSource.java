package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.util.*;


@Mod(modid = "DynamicLights_mobEquipment", name = "Dynamic Lights on Mob Equipment", version = "1.0.0", dependencies = "required-after:DynamicLights")
public class EntityLivingEquipmentLightSource {
    private Minecraft mcinstance;
    private long nextUpdate;
    private long updateInterval;
    private ArrayList<EntityLightAdapter> trackedEntities;
    private Thread thread;
    private boolean threadRunning;
    private HashMap itemsMap;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        config.load();

        Property updateI = config.get("general", "update Interval", 1000);
        updateI.comment = "Update Interval time for all EntityLiving in milliseconds. The lower the better and costlier.";
        this.updateInterval = updateI.getInt();

        this.itemsMap = new HashMap();
        Property itemsList = config.get("general", "LightItems", "50:15,89:12,348:10,91:15,327:15,76:10,331:10,314:14");
        itemsList.comment = "Item and Armor IDs that shine light when found on any EntityLiving. Syntax: ItemID:LightValue, seperated by commas";
        String[] tokens = itemsList.getString().split(",");
        for (String pair : tokens) {
            String[] values = pair.split(":");
            int id = Integer.valueOf(values[0]).intValue();
            int value = Integer.valueOf(values[1]).intValue();
            this.itemsMap.put(Integer.valueOf(id), Integer.valueOf(value));
        }

        config.save();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        this.mcinstance = FMLClientHandler.instance().getClient();
        this.nextUpdate = System.currentTimeMillis();
        this.trackedEntities = new ArrayList();
        this.threadRunning = false;
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
    }

    private boolean hasShinyEquipment(EntityLiving ent) {
        return (getLightFromItemStack(ent.getCurrentItemOrArmor(0)) > 0) || (getLightFromItemStack(ent.getCurrentItemOrArmor(4)) > 0);
    }

    private int getLightFromItemStack(ItemStack stack) {
        if (stack != null) {
            Integer i = (Integer) this.itemsMap.get(Integer.valueOf(stack.itemID));
            if (i != null) {
                return i.intValue();
            }
        }
        return 0;
    }

    private class EntityLightAdapter
            implements IDynamicLightSource {
        private EntityLiving entity;
        private int lightLevel;
        private boolean enabled;

        public EntityLightAdapter(EntityLiving e) {
            this.lightLevel = 0;
            this.enabled = false;
            this.entity = e;
        }

        public void onTick() {
            this.lightLevel = Math.max(EntityLivingEquipmentLightSource.this.getLightFromItemStack(this.entity.getCurrentItemOrArmor(0)), EntityLivingEquipmentLightSource.this.getLightFromItemStack(this.entity.getCurrentItemOrArmor(4)));

            if (this.entity.getEntityData().getString("InfernalMobsMod") != null) {
                this.lightLevel = 15;
            }

            if ((!this.enabled) && (this.lightLevel > 8)) {
                enableLight();
            } else if ((this.enabled) && (this.lightLevel < 9)) {
                disableLight();
            }
        }

        private void enableLight() {
            DynamicLights.addLightSource(this);
            this.enabled = true;
        }

        private void disableLight() {
            DynamicLights.removeLightSource(this);
            this.enabled = false;
        }

        public Entity getAttachmentEntity() {
            return this.entity;
        }

        public int getLightLevel() {
            return this.lightLevel;
        }
    }

    private class EntityListChecker extends Thread {
        private final Object[] list;

        public EntityListChecker(List input) {
            this.list = input.toArray();
        }

        public void run() {
            ArrayList newList = new ArrayList();

            for (Object o : this.list) {
                Entity ent = (Entity) o;

                if ((!(ent instanceof EntityLiving)) || (!ent.isEntityAlive()) || ((ent instanceof EntityPlayer)) || (!EntityLivingEquipmentLightSource.this.hasShinyEquipment((EntityLiving) ent))) {
                    continue;
                }
                boolean found = false;
                Iterator iter = EntityLivingEquipmentLightSource.this.trackedEntities.iterator();
                EntityLivingEquipmentLightSource.EntityLightAdapter adapter = null;
                while (iter.hasNext()) {
                    adapter = (EntityLivingEquipmentLightSource.EntityLightAdapter) iter.next();
                    if (!adapter.getAttachmentEntity().equals(ent))
                        continue;
                    adapter.onTick();
                    newList.add(adapter);
                    found = true;
                    iter.remove();
                }

                if (found) {
                    continue;
                }
                adapter = new EntityLivingEquipmentLightSource.EntityLightAdapter((EntityLiving) ent);
                adapter.onTick();
                newList.add(adapter);
            }

            for (EntityLivingEquipmentLightSource.EntityLightAdapter adapter : EntityLivingEquipmentLightSource.this.trackedEntities) {
                adapter.onTick();
            }

            trackedEntities = newList;
            threadRunning = false;
        }
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if ((EntityLivingEquipmentLightSource.this.mcinstance.theWorld != null) && (System.currentTimeMillis() > EntityLivingEquipmentLightSource.this.nextUpdate) && (!DynamicLights.globalLightsOff())) {
                nextUpdate = (System.currentTimeMillis() + EntityLivingEquipmentLightSource.this.updateInterval);

                if (!EntityLivingEquipmentLightSource.this.threadRunning) {
                    thread = new EntityLivingEquipmentLightSource.EntityListChecker(EntityLivingEquipmentLightSource.this.mcinstance.theWorld.loadedEntityList);
                    EntityLivingEquipmentLightSource.this.thread.setPriority(1);
                    EntityLivingEquipmentLightSource.this.thread.start();
                    threadRunning = true;
                }
            }
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights_onFire";
        }
    }
}