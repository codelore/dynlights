package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import atomicstryker.dynamiclights.client.ItemConfigHelper;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.util.EnumSet;


@Mod(modid = "DynamicLights_thePlayer", name = "Dynamic Lights Player Light", version = "1.0.8", dependencies = "required-after:DynamicLights")
public class PlayerSelfLightSource
        implements IDynamicLightSource {
    private EntityPlayer thePlayer;
    private World lastWorld;
    private int lightLevel;
    private boolean enabled;
    private ItemConfigHelper itemsMap;
    private ItemConfigHelper notWaterProofItems;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        config.load();

        Property itemsList = config.get("general", "LightItems", "50,89=12,348=10,91,327,76=10,331=10,314=14");
        itemsList.comment = "Item IDs that shine light while held. Armor Items also work when worn. [ONLY ON YOURSELF]";
        this.itemsMap = new ItemConfigHelper(itemsList.getString(), 15);

        Property notWaterProofList = config.get("general", "TurnedOffByWaterItems", "50,327");
        notWaterProofList.comment = "Item IDs that do not shine light when held in water, have to be present in LightItems.";
        this.notWaterProofItems = new ItemConfigHelper(notWaterProofList.getString(), 1);

        config.save();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        this.lightLevel = 0;
        this.enabled = false;
        this.lastWorld = null;

        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
    }

    private int getLightFromItemStack(ItemStack stack) {
        int xlight = getXLightFromItemStack(stack);
        if (xlight > 0)
            return xlight;

        if (stack != null) {
            int r = this.itemsMap.retrieveValue(stack.itemID, stack.getItemDamage());
            return r < 0 ? 0 : r;
        }
        return 0;
    }

    private int getXLightFromItemStack(ItemStack stack) {
        if (stack == null)
            return 0;

        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null && compound.hasKey("xlight")) {
            NBTTagCompound xlight = compound.getCompoundTag("xlight");
            return xlight.getInteger("level");
        }
        return 0;
    }


    private void enableLight() {
        DynamicLights.addLightSource(this);
        this.enabled = true;
    }

    private void disableLight() {
        DynamicLights.removeLightSource(this);
        this.enabled = false;
    }

    public Entity getAttachmentEntity() {
        return this.thePlayer;
    }

    public int getLightLevel() {
        return this.lightLevel;
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if ((PlayerSelfLightSource.this.lastWorld != FMLClientHandler.instance().getClient().theWorld) || (PlayerSelfLightSource.this.thePlayer != FMLClientHandler.instance().getClient().thePlayer)) {
                thePlayer = FMLClientHandler.instance().getClient().thePlayer;
                if (PlayerSelfLightSource.this.thePlayer != null) {
                    lastWorld = (PlayerSelfLightSource.this.thePlayer.worldObj);
                } else {
                    lastWorld = (null);
                }
            }

            if ((PlayerSelfLightSource.this.thePlayer != null) && (PlayerSelfLightSource.this.thePlayer.isEntityAlive()) && (!DynamicLights.globalLightsOff())) {
                int prevLight = PlayerSelfLightSource.this.lightLevel;

                ItemStack item = PlayerSelfLightSource.this.thePlayer.getCurrentEquippedItem();
                lightLevel = (PlayerSelfLightSource.this.getLightFromItemStack(item));

                for (ItemStack armor : PlayerSelfLightSource.this.thePlayer.inventory.armorInventory) {
                    lightLevel = (Math.max(PlayerSelfLightSource.this.lightLevel, PlayerSelfLightSource.this.getLightFromItemStack(armor)));
                }

                if ((prevLight != 0) && (PlayerSelfLightSource.this.lightLevel != prevLight)) {
                    lightLevel = (0);
                } else if (PlayerSelfLightSource.this.thePlayer.isBurning()) {
                    lightLevel = (15);
                } else if ((checkPlayerWater(PlayerSelfLightSource.this.thePlayer)) && (item != null) && (PlayerSelfLightSource.this.notWaterProofItems.retrieveValue(item.itemID, item.getItemDamage()) == 1)) {
                    lightLevel = (0);

                    for (ItemStack armor : PlayerSelfLightSource.this.thePlayer.inventory.armorInventory) {
                        if ((armor == null) || (PlayerSelfLightSource.this.notWaterProofItems.retrieveValue(armor.itemID, item.getItemDamage()) != 0))
                            continue;
                        lightLevel = (Math.max(PlayerSelfLightSource.this.lightLevel, PlayerSelfLightSource.this.getLightFromItemStack(armor)));
                    }

                }

                if ((!PlayerSelfLightSource.this.enabled) && (PlayerSelfLightSource.this.lightLevel > 8)) {
                    PlayerSelfLightSource.this.enableLight();
                } else if ((PlayerSelfLightSource.this.enabled) && (PlayerSelfLightSource.this.lightLevel < 9)) {
                    PlayerSelfLightSource.this.disableLight();
                }
            }
        }

        private boolean checkPlayerWater(EntityPlayer thePlayer) {
            if (thePlayer.isInWater()) {
                int x = MathHelper.floor_double(thePlayer.posX + 0.5D);
                int y = MathHelper.floor_double(thePlayer.posY + thePlayer.getEyeHeight());
                int z = MathHelper.floor_double(thePlayer.posZ + 0.5D);
                return thePlayer.worldObj.getBlockMaterial(x, y, z) == Material.water;
            }
            return false;
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights_thePlayer";
        }
    }
}