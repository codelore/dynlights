package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;


@Mod(modid = "DynamicLights_onFire", name = "Dynamic Lights on burning", version = "1.0.2", dependencies = "required-after:DynamicLights")
public class BurningEntitiesLightSource {
    private Minecraft mcinstance;
    private long nextUpdate;
    private long updateInterval;
    private ArrayList<EntityLightAdapter> trackedEntities;
    private Thread thread;
    private boolean threadRunning;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent evt) {
        Configuration config = new Configuration(evt.getSuggestedConfigurationFile());
        config.load();

        Property updateI = config.get("general", "update Interval", 1000);
        updateI.comment = "Update Interval time for all burning EntityLiving, Arrows and Fireballs in milliseconds. The lower the better and costlier.";
        this.updateInterval = updateI.getInt();

        config.save();
    }

    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        this.mcinstance = FMLClientHandler.instance().getClient();
        this.nextUpdate = System.currentTimeMillis();
        this.trackedEntities = new ArrayList();
        this.threadRunning = false;
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
    }

    private class EntityLightAdapter
            implements IDynamicLightSource {
        private Entity entity;
        private int lightLevel;
        private boolean enabled;

        public EntityLightAdapter(Entity e) {
            this.lightLevel = 0;
            this.enabled = false;
            this.entity = e;
        }

        public void onTick() {
            if (this.entity.isBurning()) {
                this.lightLevel = 15;
            } else {
                this.lightLevel = 0;
            }

            if ((!this.enabled) && (this.lightLevel > 8)) {
                enableLight();
            } else if ((this.enabled) && (this.lightLevel < 9)) {
                disableLight();
            }
        }

        private void enableLight() {
            DynamicLights.addLightSource(this);
            this.enabled = true;
        }

        private void disableLight() {
            DynamicLights.removeLightSource(this);
            this.enabled = false;
        }

        public Entity getAttachmentEntity() {
            return this.entity;
        }

        public int getLightLevel() {
            return this.lightLevel;
        }
    }

    private class EntityListChecker extends Thread {
        private final Object[] list;

        public EntityListChecker(List input) {
            this.list = input.toArray();
        }

        public void run() {
            ArrayList newList = new ArrayList();

            for (Object o : this.list) {
                Entity ent = (Entity) o;

                if (((!(ent instanceof EntityLiving)) && (!(ent instanceof EntityFireball)) && (!(ent instanceof EntityArrow))) || (!ent.isEntityAlive()) || (!ent.isBurning()) || ((ent instanceof EntityItem)) || ((ent instanceof EntityPlayer))) {
                    continue;
                }
                boolean found = false;
                Iterator iter = BurningEntitiesLightSource.this.trackedEntities.iterator();
                BurningEntitiesLightSource.EntityLightAdapter adapter = null;
                while (iter.hasNext()) {
                    adapter = (BurningEntitiesLightSource.EntityLightAdapter) iter.next();
                    if (!adapter.getAttachmentEntity().equals(ent))
                        continue;
                    adapter.onTick();
                    newList.add(adapter);
                    found = true;
                    iter.remove();
                }

                if (found) {
                    continue;
                }
                adapter = new BurningEntitiesLightSource.EntityLightAdapter(ent);
                adapter.onTick();
                newList.add(adapter);
            }

            for (BurningEntitiesLightSource.EntityLightAdapter adapter : BurningEntitiesLightSource.this.trackedEntities) {
                adapter.onTick();
            }

            trackedEntities = newList;
            threadRunning = false;
        }
    }

    private class TickHandler
            implements ITickHandler {
        private final EnumSet ticks;

        public TickHandler() {
            this.ticks = EnumSet.of(TickType.CLIENT);
        }

        public void tickStart(EnumSet type, Object[] tickData) {
        }

        public void tickEnd(EnumSet type, Object[] tickData) {
            if ((BurningEntitiesLightSource.this.mcinstance.theWorld != null) && (System.currentTimeMillis() > BurningEntitiesLightSource.this.nextUpdate) && (!DynamicLights.globalLightsOff())) {
                nextUpdate = System.currentTimeMillis() + BurningEntitiesLightSource.this.updateInterval;

                if (!BurningEntitiesLightSource.this.threadRunning) {
                    thread = new BurningEntitiesLightSource.EntityListChecker(BurningEntitiesLightSource.this.mcinstance.theWorld.loadedEntityList);
                    BurningEntitiesLightSource.this.thread.setPriority(1);
                    BurningEntitiesLightSource.this.thread.start();
                    threadRunning = true;
                }
            }
        }

        public EnumSet ticks() {
            return this.ticks;
        }

        public String getLabel() {
            return "DynamicLights_onFire";
        }
    }
}