package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;


@Mod(modid = "DynamicLights_flameArrows", name = "Dynamic Lights on Flame enchanted Arrows", version = "1.0.0", dependencies = "required-after:DynamicLights")
public class FlameEnchantedArrowLightSource {
    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @ForgeSubscribe
    public void onEntityJoinedWorld(EntityJoinWorldEvent event) {
        if ((event.entity instanceof EntityArrow)) {
            EntityArrow arrow = (EntityArrow) event.entity;
            if ((arrow.shootingEntity != null) && ((arrow.shootingEntity instanceof EntityPlayer))) {
                EntityPlayer shooter = (EntityPlayer) arrow.shootingEntity;
                if (EnchantmentHelper.getFireAspectModifier(shooter) != 0) {
                    DynamicLights.addLightSource(new EntityLightAdapter(arrow));
                }
            }
        }
    }

    private class EntityLightAdapter implements IDynamicLightSource {
        private EntityArrow entity;
        private int lightLevel;

        public EntityLightAdapter(EntityArrow entArrow) {
            this.lightLevel = 15;
            this.entity = entArrow;
        }

        public Entity getAttachmentEntity() {
            return this.entity;
        }

        public int getLightLevel() {
            return this.lightLevel;
        }
    }
}