package atomicstryker.dynamiclights.client.modules;

import atomicstryker.dynamiclights.client.DynamicLights;
import atomicstryker.dynamiclights.client.IDynamicLightSource;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;

@Mod(modid = "DynamicLights_creepers", name = "Dynamic Lights on Creepers", version = "1.0.2", dependencies = "required-after:DynamicLights")
public class ChargingCreeperLightSource {
    @Mod.Init
    public void load(FMLInitializationEvent evt) {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @ForgeSubscribe
    public void onPlaySoundAtEntity(PlaySoundAtEntityEvent event) {
        if ((event.name != null) && (event.name.equals("random.fuse")) && (event.entity != null) && ((event.entity instanceof EntityCreeper))) {
            if (event.entity.isEntityAlive()) {
                EntityLightAdapter adapter = new EntityLightAdapter((EntityCreeper) event.entity);
                adapter.onTick();
            }
        }
    }

    private class EntityLightAdapter implements IDynamicLightSource {
        private EntityCreeper entity;
        private int lightLevel;
        private boolean enabled;

        public EntityLightAdapter(EntityCreeper eC) {
            this.lightLevel = 15;
            this.enabled = false;
            this.entity = eC;
        }

        public void onTick() {
            this.lightLevel = (this.entity.getCreeperState() == 1 ? 15 : 0);

            if ((!this.enabled) && (this.lightLevel > 8)) {
                enableLight();
            } else if ((this.enabled) && (this.lightLevel < 9)) {
                disableLight();
            }
        }

        private void enableLight() {
            DynamicLights.addLightSource(this);
            this.enabled = true;
        }

        private void disableLight() {
            DynamicLights.removeLightSource(this);
            this.enabled = false;
        }

        public Entity getAttachmentEntity() {
            return this.entity;
        }

        public int getLightLevel() {
            return this.lightLevel;
        }
    }
}